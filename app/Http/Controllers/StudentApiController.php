<?php

namespace App\Http\Controllers;

use App\Student;
use App\Subject;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Response;
use Validator;

class StudentApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        $this->authorize('list-students');

        return Student::with('user', 'subjects')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return mixed
     *
     */
    public function store()
    {
        return response('', 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return mixed
     */
    public function show($id)
    {
        $this->authorize('show-student');

        return Student::with('user', 'subjects')->findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $this->authorize('edit-student');

        // Validate the request
        Validator::make($request->all(), [
            'subjects'        => 'required|array|size:2',
            'subjects.*'      => 'required|array|size:5',
            'subjects.*.*'    => 'required|array',
            'subjects.*.*.id' => 'required|numeric',
        ])->validate();

        $fullClasses = DB::table('student_subject')
            ->select(DB::raw('subject_id, unit, count(student_id) as student_count'))
            ->groupBy('unit')
            ->groupBy('subject_id')
            ->get()
            ->where('student_count', '>=', 20);

        $student = Student::with('subjects')->find($id);

        $subjects = collect($request->subjects)->map(function ($unit, $key) {
            return collect($unit)->map(function ($subject, $index) use ($key) {
                $unit = (int)substr($key, -1);

                return [
                    'id'    => $subject['id'],
                    'name'  => $subject['name'],
                    'unit'  => $unit,
                    'index' => $index,
                ];
            });
        })->flatten(1);

        $selectedFullClasses = $fullClasses->map(function ($class) use ($subjects, $student) {
            $class = $subjects->where('id', $class->subject_id)->where('unit', $class->unit)->first();
            $existingClass = $student->subjects->where('id', $class['id'])->where('pivot.unit', $class['unit'])->first();

            $class && $class['name'] .= " unit {$class['unit']}";

            return $existingClass ? null : $class;
        })->filter(function ($class) {
            return $class !== null;
        })->values();

        // Each class can only have 20 students
        if (!$selectedFullClasses->isEmpty()) {
            if ($selectedFullClasses->count() == 1) {
                $message = $selectedFullClasses->first()['name'] . ' is full';
            } elseif ($selectedFullClasses->count() == 2) {
                $message = $selectedFullClasses->implode('name', ' and ') . ' are full';
            } else {
                $message = $selectedFullClasses->implode('name', ', ') . ' are full';
                $message = substr_replace($message, ', and ', strrpos($message, ', '), 2);
            }

            return Response::json(['message' => $message], 422);
        };

        // Subjects can only be selected once in the same unit
        if (!$subjects->groupBy('unit')->every(function (\Illuminate\Support\Collection $unit) {
            return $unit->every(function ($selection) use ($unit) {
                return $unit->where('id', $selection['id'])->count() === 1;
            });
        })
        ) {
            return Response::json(['message' => 'One or more subjects have been selected twice in the same unit'], 422);
        };

        // English and ESL cannot be selected in the same unit
        $englishId = Subject::whereName('English')->first()->id;
        $eslId = Subject::whereName('ESL')->first()->id;
        if ($subjects->where('id', $englishId)->count() >= 1 && $subjects->where('id', $eslId)->count() >= 1) {
            return Response::json(['message' => 'English and ESL cannot be selected at the same time'], 422);
        };

        $student->subjects()->detach();

        $subjects->each(function ($subject) use ($student) {
            $student->subjects()->attach($subject['id'], ['unit' => $subject['unit'], 'index' => $subject['index']]);
        });

        return Response::json($student->fresh()->toArray()['subjects']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response('', 404);
    }
}
