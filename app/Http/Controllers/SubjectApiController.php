<?php

namespace App\Http\Controllers;

use App\Student;
use App\Subject;
use Illuminate\Http\Request;

class SubjectApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request)
    {
        $this->authorize('list-subjects');

        $with = explode(',', $request->input('with'));

        if (in_array('students', $with)) {
            return Subject::with('students.user')->get();
        } else {
            return Subject::with('students')->get()->map(function (Subject $subject) {
                return $subject->attributesToArray() + [
                    'students_count' => [
                        'unit1' => $subject->students->where('pivot.unit', 1)->count(),
                        'unit2' => $subject->students->where('pivot.unit', 2)->count(),
                    ]
                ];
            });
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return response('', 404);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('show-subject');

        return Subject::with('students.user')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return response('', 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response('', 404);
    }
}
