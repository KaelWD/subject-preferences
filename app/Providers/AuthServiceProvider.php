<?php

namespace App\Providers;

use App\Teacher;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('show-user', function ($user, $target_user) {
            return $user->userable_type === Teacher::class
                || $user->id === $target_user->id;
        });

        Gate::define('edit-user', function ($user, $target_user) {
            return $user->userable_type === Teacher::class
                || $user->id === $target_user->id;
        });

        Gate::define('create-user', function ($user) {
            return $user->userable_type === Teacher::class;
        });

        Gate::define('remove-user', function ($user) {
            return $user->userable_type === Teacher::class;
        });

        Gate::define('show-student', function ($user, $student) {
            return $user->userable_type === Teacher::class
                || $user->id === $student->user_id;
        });

        Gate::define('edit-student', function ($user, $student) {
            return $user->userable_type === Teacher::class
                || $user->id === $student->user_id;
        });

        Gate::define('list-students', function ($user) {
            return $user->userable_type === Teacher::class;
        });

        Gate::define('show-subject', function ($user) {
            return $user->userable_type === Teacher::class;
        });

        Gate::define('list-subjects', function ($user) {
            return $user->userable_type === Teacher::class;
        });
    }
}
