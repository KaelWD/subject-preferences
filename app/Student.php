<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Student
 *
 * @property string                                                       $id
 * @property int                                                          $user_id
 * @property string                                                       $form_group
 * @property \Carbon\Carbon                                               $created_at
 * @property \Carbon\Carbon                                               $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Subject[] $subjects
 * @property-read \App\User                                               $user
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereFormGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Student whereUserId($value)
 * @mixin \Eloquent
 */
class Student extends Model
{
    protected $keyType = 'string';

    public $incrementing = false;

    protected $fillable = [
        'id', 'form_group',
    ];

    public function subjects()
    {
        return $this->belongsToMany(Subject::class)->withPivot('unit', 'index')->orderBy('pivot_unit')->orderBy('pivot_index');
    }

    public function toArray()
    {
        return [
            'id'         => $this->id,
            'first_name' => $this->user->first_name,
            'surname'    => $this->user->surname,
            'dob'        => $this->user->dob,
            'gender'     => $this->user->gender,
            'form_group' => $this->form_group,
            'subjects'   => [
                'unit1' => $this->subjectsByUnit(1),
                'unit2' => $this->subjectsByUnit(2),
            ],
        ];
    }

    public function subjectsByUnit(int $unit)
    {
        return $this->subjects->filter(function (Subject $subject) use ($unit) {
            return $subject->pivot->unit === $unit;
        })->map(function (Subject $subject) {
            return [
                'id' => $subject->id,
                'name' => $subject->name
            ];
        })->values();
    }

    public function user()
    {
        return $this->morphOne(User::class, 'userable');
    }
}
