<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Subject
 *
 * @property int                                                          $id
 * @property string                                                       $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Student[] $students
 * @method static \Illuminate\Database\Query\Builder|\App\Subject whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Subject whereName($value)
 * @mixin \Eloquent
 */
class Subject extends Model
{
    public function students()
    {
        return $this->belongsToMany(Student::class)->withPivot('unit');
    }

    public function toArray()
    {
        return [
            'id'       => $this->id,
            'name'     => $this->name,
            'students' => [
                'unit1' => $this->studentsByUnit(1),
                'unit2' => $this->studentsByUnit(2),
            ],
        ];
    }

    /**
     * @param int $unit
     *
     * @return \Illuminate\Support\Collection
     */
    public function studentsByUnit(int $unit)
    {
        return $this->students->filter(function (Student $student) use ($unit): bool {
            return $student->pivot->unit === $unit;
        })->map(function (Student $student) {
            return [
                'id'         => $student->id,
                'first_name' => $student->user->first_name,
                'surname'    => $student->user->surname,
                'dob'        => $student->user->dob,
                'gender'     => $student->user->gender,
                'form_group' => $student->form_group,
            ];
        })->values();
    }
}
