<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $keyType = 'string';

    public $incrementing = false;

    protected $fillable = [
        'id',
    ];

    public function user()
    {
        return $this->morphOne(User::class, 'userable');
    }
}
