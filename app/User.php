<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * App\User
 *
 * @property int                                                $id
 * @property string                                             $first_name
 * @property string                                             $surname
 * @property string                                             $dob
 * @property string                                             $gender
 * @property string                                             $password
 * @property string                                             $userable_id
 * @property string                                             $userable_type
 * @property string                                             $remember_token
 * @property \Carbon\Carbon                                     $created_at
 * @property \Carbon\Carbon                                     $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $userable
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereDob($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereSurname($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserableId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUserableType($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    protected $fillable = [
        'id', 'first_name', 'surname', 'dob', 'gender', 'password',
    ];

    protected $visible = [
        'id', 'first_name', 'surname', 'dob', 'gender',
    ];

    public function toArray()
    {
        $userable_class = explode('\\', get_class($this->userable));

        return [
            'id'         => $this->userable->id,
            'first_name' => $this->first_name,
            'surname'    => $this->surname,
            'dob'        => $this->dob,
            'gender'     => $this->gender,
            'type'       => end($userable_class),
        ];
    }

    public function userable()
    {
        return $this->morphTo();
    }
}
