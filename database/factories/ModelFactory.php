<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Student;
use App\Teacher;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    static $password;

    $gender = (bool)random_int(0, 1) ? 'male' : 'female';

    // Generate random DOB
    $age = random_int(15, 17);
    $dobYear = (int)Carbon::parse("-$age years")->format('Y');
    $dob = $faker->dateTimeBetween("january $dobYear", 'january ' . ($dobYear + 1));

    return [
        'first_name'     => $faker->firstName($gender),
        'surname'        => $faker->lastName(),
        'dob'            => $dob->format('Y-m-d'),
        'gender'         => $gender,
        'password'       => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Student::class, function (Faker $faker) {
    $surname = $faker->lastName();
    $id = strtolower(
            substr(
                preg_replace("/[^a-z]/i", '', $surname), // Remove non-letter characters from name
                0, 3                                     // First 3 characters
            )
        ) . $faker->numerify('###');
    $user = factory(User::class)->create([
        'surname'       => $surname,
        'userable_id'   => $id,
        'userable_type' => Student::class,
    ]);

    return [
        'id'         => $id,
        'user_id'    => $user->id,
        'form_group' => 10 . range('A', 'G')[random_int(0, 6)],
    ];
});

$factory->define(Teacher::class, function (Faker $faker) {
    // Generate random DOB
    $age = random_int(22, 73);
    $dobYear = (int)Carbon::parse("-$age years")->format('Y');
    $dob = $faker->dateTimeBetween("january $dobYear", 'january ' . ($dobYear + 1));

    $surname = $faker->lastName();
    $id = strtoupper(substr(
        preg_replace("/[^a-z]/i", '', $surname), // Remove non-letter characters from name
        0, 3                                     // First 3 characters
    ));
    $user = factory(User::class)->create([
        'surname'       => $surname,
        'dob'           => $dob->format('Y-m-d'),
        'userable_id'   => $id,
        'userable_type' => Teacher::class,
    ]);

    return [
        'id'      => $id,
        'user_id' => $user->id,
    ];
});
