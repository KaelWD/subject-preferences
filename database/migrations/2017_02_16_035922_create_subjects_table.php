<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        $subjects = [
            'English',
            'ESL',
            'Physics',
            'Further Mathematics',
            'General Mathematics',
            'Mathematical Methods',
            'Specialist Mathematics',
            'Biology',
            'Chemistry',
            'French',
            'Indonesian',
            'Geography',
            'Health',
            'Dance',
            'Business Management',
            'Accounting',
            'Food Technology',
            'Engineering',
            'Software Development',
            'Art',
            'Visual Communications and Design',
            'Physical Education',
        ];

        DB::table('subjects')->insert(array_map(function ($name) {
            return ['name' => $name];
        }, $subjects));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
