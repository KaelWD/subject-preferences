<?php

Use App\Student;
use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Student::class, 25)->create()->each(function (Student $student) {
            foreach (range(1, 2) as $unit) {
                DB::table('subjects')->get()
                    ->random(5)
                    ->values()
                    ->each(function ($subject, $index) use ($student, $unit) {
                        DB::table('student_subject')->insert([
                            'student_id' => $student->id,
                            'subject_id' => $subject->id,
                            'unit'       => $unit,
                            'index'      => $index,
                        ]);
                    });
            }
        });
    }
}
