<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

## Environment setup

 - Install git
 - Install PHP >7.0
 - Install Node.JS with npm
 - Install MariaDB

## Installing

```bash
git clone https://gitlab.com/KaelWD/subject-preferences.git subject-preferences
cd ./subject-preferences

# Install JS packages
# Use 'npm install --no-bin-links' on Windows
# If this fails you might have to run 'npm rebuild'
npm install

# Install PHP packages and initialise project
composer create-project

# Create database
mysql -h localhost -u root -p

CREATE DATABASE subject-preferences;
QUIT;
```

Modify the `.env` file that was generated to set environmental variables, such as database name and password. 

Create the database schema:  
`php artisan migrate`

Run database seeds for development and testing:  
`php artisan db:seed`  
You can also run `php artisan migrate --seed` to do both with a single command. 

## Building

To compile the JS files, run one of
```bash
npm run dev         # Includes debugging code, very large files
npm run watch       # Same as above, but will automatically refresh on modifications
npm run production  # Production mode, minifies and removes debug code
```

You can now either point you web server of choice to the project directory (remember Laravel uses a front controller so set the document root accordingly), or simply run `php artisan serve` to use PHP's built-in server. 

## nginx config

### nginx.conf

```nginx
http {
    default_type  application/octet-stream;
    
    server_name_in_redirect  off;
    server_tokens            off;
    
    upstream php {
        server  127.0.0.1:9100;
    }
    
    server {
        listen       80;
        server_name  subject-preferences.dev;        # Change this
        root         '~/subject-preferences/public'; # This too
        
        index      index.php  index.html  index.htm;
        try_files  $uri  $uri/  @rewrite;
        
        location @rewrite {
            rewrite  ^/(.*)$  /index.php?_url=/$1;
        }
        
        location ~ \.php$ {
            try_files      $uri =404;
            fastcgi_pass   php;
            fastcgi_index  index.php;
            fastcgi_param  SCRIPT_FILENAME  $request_filename;
            fastcgi_param  REMOTE_ADDR      $http_x_real_ip;
            include        fastcgi_params;
        }
    }
```

### fastcgi_params

```nginx
fastcgi_param  QUERY_STRING       $query_string;
fastcgi_param  REQUEST_METHOD     $request_method;
fastcgi_param  CONTENT_TYPE       $content_type;
fastcgi_param  CONTENT_LENGTH     $content_length;

fastcgi_param  SCRIPT_NAME        $fastcgi_script_name;
fastcgi_param  REQUEST_URI        $request_uri;
fastcgi_param  DOCUMENT_URI       $document_uri;
fastcgi_param  DOCUMENT_ROOT      $document_root;
fastcgi_param  SERVER_PROTOCOL    $server_protocol;
fastcgi_param  REQUEST_SCHEME     $scheme;
fastcgi_param  HTTPS              $https if_not_empty;

fastcgi_param  GATEWAY_INTERFACE  CGI/1.1;
fastcgi_param  SERVER_SOFTWARE    nginx/$nginx_version;

fastcgi_param  REMOTE_ADDR        $remote_addr;
fastcgi_param  REMOTE_PORT        $remote_port;
fastcgi_param  SERVER_ADDR        $server_addr;
fastcgi_param  SERVER_PORT        $server_port;
fastcgi_param  SERVER_NAME        $server_name;

# PHP only, required if PHP was built with --enable-force-cgi-redirect
fastcgi_param  REDIRECT_STATUS    200;
```

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
