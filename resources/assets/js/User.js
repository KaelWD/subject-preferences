export default class {
    constructor(id, name, role) {
        this.id = id;
        this.name = name;
        this.role = role;
    }
}
