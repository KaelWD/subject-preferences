import router from './router';
import User from './User.js';

require('./bootstrap');

window.store = new Vue({
    data() {
        let user;
        if (window.Laravel.user.role === 'guest') {
            user = new User('', '', 'guest');
        } else {
            user = new User(
                window.Laravel.user.id,
                `${window.Laravel.user.first_name} ${window.Laravel.user.surname}`,
                window.Laravel.user.role
            )
        }
        return {
            user: user,
        }
    },
    created() {
        this.fetchUser();
    },
    methods: {
        fetchUser() {
            window.axios.get('/api/user').then(r => {
                this.user = new User(
                    r.data.id,
                    r.data.first_name + ' ' + r.data.surname,
                    r.data.type.toLowerCase()
                );
            }, () => {
                this.user.role = 'guest';
            });
        },
        doLogin(username, password, remember, redirect) {
            return window.axios.post(`/login`, {
                userId: username,
                password: password,
                remember: remember
            }).then(r => {
                this.user = new User(
                    r.data.id,
                    r.data.first_name + ' ' + r.data.surname,
                    r.data.type.toLowerCase()
                );
                return true;
            }, err => {
                return Promise.reject(err.response);
            });
        },
        doLogout() {
            window.axios.post(`/logout`).then(r => {
                this.user = new User('', '', 'guest');
                app.$router.push('/');
            })
        }
    }
});

window.routeRequiresRole = function (route, role) {
    return route.matched.some(record => record.meta.requiresRole === role);
};

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // Route requires authentication
        if (routeRequiresRole(to, window.store.user.role) || routeRequiresRole(to, undefined)) {
            // Already authenticated
            next();
        } else {
            next({
                name: 'login',
                params: {redirect: to.fullPath}
            })
        }
    } else { // Public route
        next();
    }
});

window.app = new Vue({
    el: '#app',
    render(r) {
        return r('app');
    },
    router,
    components: {
        app: require('./app.vue')
    },
});
