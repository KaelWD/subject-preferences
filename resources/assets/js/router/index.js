import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const authGuard = {
    teacher: {
        requiresAuth: true,
        requiresRole: 'teacher'
    },
    student: {
        requiresAuth: true,
        requiresRole: 'student'
    }
};

const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'active',
    routes: [
        {
            path: '/subjects',
            name: 'subjects',
            component: require('./pages/subjects.vue'),
            meta: authGuard.teacher
        },
        {
            path: '/subjects/:subjectId',
            name: 'subject',
            component: require('./pages/subject.vue'),
            props: true,
            meta: authGuard.teacher
        },
        {
            path: '/students',
            name: 'students',
            component: require('./pages/students.vue'),
            meta: authGuard.teacher
        },
        {
            path: '/students/:studentId',
            name: 'student',
            component: require('./pages/student.vue'),
            props: true,
            meta: authGuard.teacher
        },
        {
            path: '/students/:studentId/edit',
            name: 'editStudent',
            component: require('./pages/edit-student-profile.vue'),
            props: true,
            meta: authGuard.teacher
        },
        {
            path: '/user/edit',
            name: 'editSelf',
            component: require('./pages/edit-self.vue'),
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/user/new',
            name: 'createUser',
            component: require('./pages/create-user.vue'),
            meta: authGuard.teacher
        },
        {
            path: '/preferences',
            name: 'preferences',
            component: require('./pages/preferences.vue'),
            meta: authGuard.student
        },
        {
            path: '/login',
            name: 'login',
            component: require('./pages/login.vue'),
            props: true
        },
        {
            path: '/register',
            name: 'register'
        },
        {
            path: '/',
            name: 'index',
            component: require('./pages/index.vue')
        }
    ]
});

export default router;
export { authGuard };

/*

Required        User        Action
    any         any         next()
student         student     next()
student         guest       login
teacher         student     error

 */
