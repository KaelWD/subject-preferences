<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Subject Preferences</title>

    <link rel="stylesheet" href="{{ url(mix('/css/app.css')) }}">
</head>
<body>

    <div id="app"></div>

    <script>
        window.Laravel = {
            csrfToken: '{{ csrf_token() }}',
            user: {
                id: '{{ $user['id'] }}',
                first_name: '{{ $user['first_name'] }}',
                surname: '{{ $user['surname'] }}',
                role: '{{ strtolower($user['type']) }}'
            }
        };
    </script>
    <script src="{{ url(mix('/js/app.js')) }}"></script>
</body>
</html>
