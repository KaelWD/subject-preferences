<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$crudMethods = ['index', 'store', 'show', 'update', 'destroy'];

Route::resource('students', 'StudentApiController', ['only' => $crudMethods]);

Route::resource('subjects', 'SubjectApiController', ['only' => $crudMethods]);

Route::get('user', function () {
    return Response::json(Auth::user());
})->middleware('auth');

Route::get('users/{userId}', function (string $userId) {
    $user = App\User::whereUserableId($userId)->first();

    Gate::authorize('show-user', $user);

    return Response::json($user);
})->middleware('auth');

Route::patch('users/{userId}', function (Request $request, string $userId) {
    $user = App\User::whereUserableId($userId)->first();

    Gate::authorize('edit-user', $user);

    return Response::json($user->update($request->all()));
})->middleware('auth');

Route::any('{fallback?}', function () {
    return response([], 404);
})->where(['fallback' => '.*']);
