<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('login', function (Illuminate\Http\Request $request) {
    if (Auth::attempt([
        'userable_id' => $request->userId,
        'password'    => $request->password,
    ], $request->remember)
    ) return Response::json(Auth::user());
    throw new \Illuminate\Auth\AuthenticationException;
});

Route::post('logout', function () {
    Auth::logout();

    return ['success' => true];
})->middleware('auth');

Route::any('{fallback?}', function () {
    if ($user = Auth::user()) {
        $user = $user->toArray();
    } else {
        $user = [
            'id'         => '',
            'first_name' => '',
            'surname'    => '',
            'type'       => 'guest',
        ];
    }

    return view('app')->with('user', $user);
})->where(['fallback' => '.*']);
